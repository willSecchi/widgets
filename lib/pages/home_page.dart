import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_novo/pages/drawer_list.dart';
import 'package:flutter_app_novo/pages/hello_listview.dart';
import 'package:flutter_app_novo/pages/hello_page2.dart';
import 'package:flutter_app_novo/pages/hello_page3.dart';
import 'package:flutter_app_novo/utils/nav.dart';
import 'package:flutter_app_novo/widgets/blue_button.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Hello Flutter"),
          bottom: TabBar(
            tabs: [
              Tab(text: "TAB 1",),
              Tab(text: "TAB 2",),
              Tab(text: "TAB 3",)
            ],
          ),
        ),
        body: TabBarView(
          children: [
            _body(context),
            Container(
              color: Colors.green,
            ),
            Container(
              color: Colors.yellow,
            ),
          ],
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FloatingActionButton(
              child: Icon(Icons.favorite),
              onPressed: () {
                print("Adicionar");
              },
            ),
          ],
        ),
        drawer: DrawerList()
      ),
    );
  }
}

_body(context) {
  return Container(
    color: Colors.white,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _text(),
        _pageView(),
        _buttons(context),
      ],
    ),
  );
}

Container _pageView() {
  return Container(
    margin: EdgeInsets.only(top: 20, bottom: 20),
    height: 300,
    child: PageView(
      children: <Widget>[
        _img("assets/images/dog1.png"),
        _img("assets/images/dog2.png"),
        _img("assets/images/dog3.png"),
        _img("assets/images/dog4.png"),
        _img("assets/images/dog5.png")
      ],
    ),
  );
}

_buttons(context) {
  return Builder(
    builder: (BuildContext context) {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              BlueButton("ListView",
                  onPressed: () => _onClickNavigator(context, HelloListView())),
              BlueButton("Page 2",
                  onPressed: () => _onClickNavigator(context, HelloPage2())),
              BlueButton("Page 3",
                  onPressed: () => _onClickNavigator(context, HelloPage3())),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              BlueButton("Snack", onPressed: () => _onClickSnack(context)),
              BlueButton("Dialog", onPressed: () => _onClickDialog(context)),
              BlueButton("Toast", onPressed: _onClickToast)
            ],
          ),
        ],
      );
    },
  );
}

void _onClickNavigator(BuildContext context, Widget page) async {
  String s = await push(context, page);

  print(">> $s");
}

_onClickSnack(context) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      action: SnackBarAction(
        textColor: Colors.blue,
        label: "OK",
        onPressed: () {},
      ),
      content: Text("Olá Flutter"),
    ),
  );
}

_onClickDialog(BuildContext context) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return WillPopScope(
          child: AlertDialog(
            title: Text("Flutter é...."),
            actions: [
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Cancelar"),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  print("OK !!!!");
                },
                child: Text("Ok"),
              ),
            ],
          ),
        );
      });
}

_onClickToast() {
  Fluttertoast.showToast(
      msg: "Exemplo de toast",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 1,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 16);
}

_img(String img) {
  return Image.asset(
    img,
    fit: BoxFit.cover,
  );
}

_text() {
  return Text(
    "Hello World",
    style: TextStyle(
      fontSize: 30,
      color: Colors.blue,
      fontWeight: FontWeight.bold,
      fontStyle: FontStyle.italic,
      decoration: TextDecoration.underline,
      decorationColor: Colors.red,
      decorationStyle: TextDecorationStyle.wavy,
    ),
  );
}
